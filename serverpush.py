#!/usr/bin/python
import socket, sys

#List of commands possible:
#1 - shutdown, 2 - pause, 3 - logoff, 4 - message, 5 - time, 6 - kick

if (len(sys.argv) > 2):
	host = sys.argv[1]	
			
	if( len(sys.argv) == 4 ):
		if( sys.argv[2] == '4'):
			message  = sys.argv[2] + ':!:' + sys.argv[3]
			print message
		elif( sys.argv[2] == '5'):
			message = sys.argv[2] + ':!:' + sys.argv[3]
		else:
			print "Missing required variables" 
	else:
		message = sys.argv[2]
else:
	print "Missing required variables"
	exit()

print message

s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
s.connect( (host, 5671) )
sslSocket = socket.ssl(s)
sslSocket.write(message)

s.close

