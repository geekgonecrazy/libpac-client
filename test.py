import pty
import shlex
import os
import time
import subprocess
 
 
# prepare cmd that will start gdb in a screen session
# Note: it's actually two nested screen sessions - see text for explanation
cmdString = "xinit -- :0 /home/vagrant/libpac/client/login.py"
 
 
# allocate new pseudo-terminal and spawn screen sessions
(master, slave) = pty.openpty()
cmdArgs = shlex.split(cmdString)
p = subprocess.Popen(cmdArgs, close_fds = True, shell=False,
        stdin=slave, stdout=slave, stderr=slave)
 
 
raw_input("\nPress enter to magically quit gdb and the screen session")
 
 
# send commands to the screen session to interrupt xclock
# and quit gdb
screenCmds = [
    ]
for cmd in screenCmds :
    completeCmd = "screen -X -S xc eval \"%s\"" % cmd
    args = shlex.split(completeCmd)
    subprocess.call(args)
    #time.sleep(1) # uncomment this to see the magic unfold
