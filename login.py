#!/bin/python

###############################################
# Created by:  Aaron Ogle                     #
# Date: 08/20/2011                            #
#                                             #
#                                             #
#                                             #
###############################################
import gtk
import string
import ctypes
import os
import time
import datetime
import threading
import gobject
import sys
import socket, sys
import random
import time
from subprocess import call
#from OpenSSL import SSL



#-- Start of loginwindow class
class LoginWindow():

	def __init__(self):
		self.debug = False
		date = datetime.datetime.now()
		print "[Libpac Client Started] - " + date.strftime("%B %d, %Y at %H:%M:%S")		

		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_title("Libpac Login")
		self.window.set_border_width(10)
		self.window.fullscreen()
		
		self.logo_image = gtk.Image()
		self.logo_image.set_from_file("logo.png")

		self.loginbutton = gtk.Button('Login')

		self.username = gtk.Entry()
		self.password = gtk.Entry()
		self.password.set_visibility(False)
		self.password.set_invisible_char('*')
		
		self.logobox = gtk.HBox()
		self.logobox.pack_start(self.logo_image, False)

		self.loginfield = gtk.VBox()
		self.loginfield.pack_start(self.username, False)
		self.loginfield.pack_start(self.password, False)
		self.loginfield.pack_start(self.loginbutton, False)
		self.username.grab_focus()
		
		self.logintext = gtk.VBox()
		self.usernametext = gtk.Label('Username: ')
		self.passwordtext = gtk.Label('Password: ')
		self.logintext.pack_start(self.usernametext, False)
		self.logintext.pack_start(self.passwordtext, False)

		self.loginbox = gtk.HBox()
		self.loginbox.pack_start(self.logobox, False)
		self.loginbox.pack_start(self.logintext, False)
		self.loginbox.pack_start(self.loginfield, False)

		self.calign = gtk.Alignment(0.5,0.5,0,0)
		self.calign.add(self.loginbox)

		self.window.add(self.calign)

		self.window.show_all()

		#self.logo.connect("clicked", self.homeclicked)

		self.loginbutton.connect("clicked", self.loginbuttonclicked)
		self.window.connect("key-press-event", self.keypress)

		self.internet_image = gtk.Image()
		self.internet_image.set_from_file("internet.png")
		self.internet_image.set_pixel_size(10)

		self.internetbutton = gtk.Button()
		self.internetbutton.add(self.internet_image)

		self.office_image = gtk.Image()
		self.office_image.set_from_file("Book.png")

		self.officebutton = gtk.Button()
		self.officebutton.add(self.office_image)

		self.backbutton = gtk.Button("Back")

		self.optionpage = gtk.VBox()
		self.optionpage.pack_start(self.internetbutton, False)
		self.optionpage.add(self.officebutton)
		self.optionpage.add(self.backbutton)

		self.serverip = '10.112.28.13'
		self.serverport = 1238

		#self.backbutton.connect("clicked", self.back)


	def main(self):
		gtk.main()

	def keypress(self, widget, event) :
		if event.keyval == gtk.keysyms.Return :
			self.checklogin()

	def homeclicked(self,btn):
		self.sethome()

	def loginbuttonclicked(self,btn):

		call("/home/vagrant/libpac/client/start-session")
		#self.checklogin()
	

	def checklogin(self):
		gotuser = self.username.get_text()
		gotpass = self.password.get_text()
		if (self.debug ==True):
			self.window.hide()
			self.timerinit(11)
		s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
		
		s.connect((self.serverip, self.serverport))
		sslSocket = socket.ssl(s)
		sslSocket.write('1:!:'+gotuser + ':!:' + gotpass)
		res = sslSocket.read()
		s.close()
		
		res = res.split(':!:')
		if res[0] == '1':
			print "[User:" + gotuser + " correctly authenticated]"
			#self.showoptions()
			time = res[1]
			self.sessionid = res[2]
			self.window.hide()
			self.timerinit(time)
		else:
			dialog = gtk.MessageDialog(self.window, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE, 'Invalid username/password please try again')
			dialog.run()
			dialog.destroy()
			self.username.set_text('')
			self.password.set_text('')
			self.username.grab_focus()

	def showoptions(self):
		#self.window.remove(self.calign)
		self.calign.remove(self.loginbox)
		self.calign.add(self.optionpage)
		self.window.show_all()
		print "[**Changed to mode select**]"

	def timerinit(self, time):
		self.timedialog = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.timedialog.set_size_request(220,75)
		self.timedialog.set_title("Time remaining")
		self.timedialog.set_decorated(True)
		self.timer = Timer(time)
		self.timer.start()
		self.timeleftlabel = gtk.Label(time)
		self.hbox = gtk.HBox()
		self.hidetime = gtk.Button("Hide")
		self.hbox.pack_start(self.timeleftlabel, True, True, 0)
		self.hbox.add(self.hidetime)
		self.timedialog.add(self.hbox)
		self.timedialog.show()
		self.timedialog.show_all()
		
		self.hidetime.connect("clicked", self.hidetimer)
		self.timedialog.connect("destroy", self.hidetimer)
		self.staticon = gtk.StatusIcon() 
		self.staticon.set_from_stock(gtk.STOCK_ABOUT) 
		self.staticon.connect("popup-menu", self.showtimer) 
		self.staticon.set_tooltip("Time Remaining")
		self.timedialog.run()
        	self.staticon.set_visible(True) 

	def hidetimer(self,widget):
		self.timedialog.hide()

	def showtimer(self,button,widget, data=None):
		self.timedialog.show()
	def showtime(self):
		self.timedialog.show()
		
	def logoff(self):
		s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
		
		s.connect((self.serverip, self.serverport))
		sslSocket = socket.ssl(s)
		sslSocket.write('0:!:'+self.sessionid)
		s.close()
		self.username.set_text('')
                self.password.set_text('')
                self.username.grab_focus()
		self.window.show_all()
		self.timedialog.destroy()

	def destroy(self):
		date = datetime.datetime.now()
		self.timedialog.destroy()
		print "[Libpac Client Ended] - " + date.strftime("%B %d, %Y at %H:%M:%S")
		gtk.main_quit()


class Timer(threading.Thread):
	def __init__(self,time):
		self.timeleft = int(time)*60
		print self.timeleft
		threading.Thread.__init__(self)
		self.stopthread = threading.Event()

	def run(self):
		while not self.stopthread.isSet() :
			global app
			gtk.threads_enter()
			if(self.timeleft == 10*60):
				message = "Notice:\n" + str(self.timeleft/60) + " Minutes remaining!"
				app.showtime()
			elif(self.timeleft == 5*60):
				message = "Notice:\n" + str(self.timeleft/60) + " Minutes remaining!" + "\nContact staff for more time"
				app.showtime()
			elif(self.timeleft == 0):
				app.timedialog.destroy()
				app.logoff()
				self.stopthread.set()
			else:
				message = str(self.timeleft/60) + " Minutes remaining!"

			app.timeleftlabel.set_text(message)
			while gtk.events_pending():
                		gtk.main_iteration()
			gtk.threads_leave()
			print self.timeleft
			time.sleep(60)
			self.timeleft -=60
			


			
			


#-- End of LoginWindow class --#

#-- ServerListen class --#
class ProcessCommand (threading.Thread):
	def __init__(self, sock):
		self.sock = sock
		threading.Thread.__init__(self)

	def run(self):
		str = self.sock.recv(100)
		command = str.split()[0][0]
		#handle code here..
		#1 - shutdown, 2 - pause, 3 - logoff, 4 - message, 5 - time, 6 - kick
		if (command == '1'):
			print "Shutdown!"
			global server
			server.stop()
			global app
			gtk.threads_enter()
			app.destroy()
			gtk.threads_leave()

		if (command == '2'):
			print "Pause"
		if (command == '3'):
			global app
			gtk.threads_enter()
			app.logoff()
			gtk.threads_leave()
		if (command == '4'):
			message = str.split(':!:',2)[1]
			global app
			gtk.threads_enter()
			messagedialog = gtk.MessageDialog(app.window, 0, gtk.MESSAGE_WARNING, gtk.BUTTONS_OK_CANCEL, message)
        		messagedialog.run()
        		messagedialog.destroy()
			gtk.threads_leave()

		if (command == '5'):
			global app
			gtk.threads_enter()
			time = str.split(':!:',2)[1]
			timemessage = time[1:] + ' Minutes have been '
			if (time[0] == '+'):
				timemessage += 'added!'
				app.timer.timeleft += int(time[1:])*60

			elif (time[0] == '-'):
				timemessage += 'subtracted!'
				app.timer.timeleft -= int(time[1:])*60
			timerem = app.timer.timeleft/60
			timestring = repr(timerem)
			app.timeleftlabel.set_text(timestring + " Minutes remaining")
			timedialog = gtk.MessageDialog(app.window, 0, gtk.MESSAGE_WARNING, gtk.BUTTONS_OK_CANCEL, timemessage)
        		timedialog.run()
        		timedialog.destroy()
			gtk.threads_leave()
			
		if (command == '6'):
			print "kick"
		self.sock.close()
		return

class ServerListener (threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.stopthread = threading.Event()
	
	def run(self):
		while not self.stopthread.isSet() :
			print "I'm here!"
			threads = []
		
			context = SSL.Context(SSL.SSLv23_METHOD)
			context.use_privatekey_file('key')
			context.use_certificate_file('cert')

			s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
			s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			s = SSL.Connection(context, s)
			s.bind( ('', 5671) )
			s.listen(5)

			while not self.stopthread.isSet() :
				sock, (remhost, remport) = s.accept()
				newThread = ProcessCommand(sock)
				threads.append(newThread.start() )
				
	def stop(self):
		print "In Stop Thread"
		self.stopthread.set()
		



#-- Main --#

if __name__ == "__main__":
	gtk.threads_init()
	server = ServerListener()
	server.start()
	app = LoginWindow()
	
	app.main()
	gtk.gdk.threads_leave()
	

#-- EOF --#

